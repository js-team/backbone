# Installation
> `npm install --save @types/backbone`

# Summary
This package contains type definitions for Backbone (http://backbonejs.org/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/backbone.

### Additional Details
 * Last updated: Wed, 22 Dec 2021 17:01:23 GMT
 * Dependencies: [@types/underscore](https://npmjs.com/package/@types/underscore), [@types/jquery](https://npmjs.com/package/@types/jquery)
 * Global values: `Backbone`

# Credits
These definitions were written by [Boris Yankov](https://github.com/borisyankov), [Natan Vivo](https://github.com/nvivo), [kenjiru](https://github.com/kenjiru), [jjoekoullas](https://github.com/jjoekoullas), [Julian Gonggrijp](https://github.com/jgonggrijp), [Kyle Scully](https://github.com/zieka), [Robert Kesterson](https://github.com/rkesters), and [Bulat Khasanov](https://github.com/khasanovbi).
